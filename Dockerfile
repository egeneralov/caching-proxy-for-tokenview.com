FROM python:3

WORKDIR /app

ADD requirements.txt /app/

RUN pip3 install --no-cache-dir -r /app/requirements.txt

ADD . .

ENV PORT=8080

CMD python app.py
