import os
import json
import base64
from time import sleep

import requests
import redis
from flask import Flask, jsonify, Response

app = Flask(__name__)

favicon = base64.b64decode("R0lGODlhEAAQAPEAAAAAAP8QCAAAAAAAACH5BAlkAAIAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAEAAQAAAC55QkIiIiIoQQQgghhBBCCCEIgiAIgiAIggAAgCAIgiAIgiAIAiAIAiAQCAQCgUAgEAAEAoFAABAIBAKBQCAQAAQCgUAAEAgEAoFAIBAABAKBQAAQCAQCgUAgEAAEAoFAACAgICAgICAgIAAgICAgIAAgICAgICAgICAAICAgICAAICAgICAgICAgACAQEBAgACAgICAgICAgIAAgEBAQIAAgICAgICAgICAAICAgICAAICAgICAgICAgIAAAAAAAICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQIAAAQIKAAAh+QQJZAACACwAAAAAEAAQAIEAAAD/+wgAAAAAAAAC55QkIiIiIoQQQgghhBBCCCEIgiAIgiAIggAAgCAIgiAIgiAIAiAIAiAQCAQCgUAgEAAEAoFAABAIBAKBQCAQAAQCgUAAEAgEAoFAIBAABAKBQAAQCAQCgUAgEAAEgUBAACAgICAgICAgIAAgEBAQIAAgICAgICAgICAAICAgICAAICAgICAgICAgACAQEBAgACAgICAgICAgIAAgEBAQIAAgICAgICAgICAAICAgICAAICAgICAgICAgIAAAAAAAICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQIAAAQIKAAAh+QQJZAACACwAAAAAEAAQAIEAAAA1/wgAAAAAAAAC55QkIiIiIoQQQgghhBBCCCEIgiAIgiAIggAAgCAIgiAIgiAIAiAIAiAQCAQCgUAgEAAEgUBAABAIBAKBQCAQAASBQEAAEAgEAoFAIBAABAKBQAAQCAQCgUAgEAAEgUBAACAgICAgICAgIAAgEBAQIAAgICAgICAgICAAICAgICAAICAgICAgICAgACAQEBAgACAgICAgICAgIAAgEBAQIAAgICAgICAgICAAICAgICAAICAgICAgICAgIAAAAAAAICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQIAAAQIKAAAh+QQJZAABACwAAAAAEAAQAIAAAAAAAAAC50wSEREREUIIIYQQQgghhBAEQRAEQRAEQQAAQBAEQRAEQRAEARAEARAIBAKBQCAQCAACgUAgAAgEAoFAIBAIAAKBQCAACAQCgUAgEAgAAoFAIAAIBAKBQCAQCAACgUAgABAQEBAQEBAQEAAQEBAQEAAQEBAQEBAQEBAAEBAQEBAAEBAQEBAQEBAQABAQEBAQABAQEBAQEBAQEAAQEBAQEAAQEBAQEBAQEBAAEBAQEBAAEBAQEBAQEBAQEAAAAAAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQIECAAAEKAAA7")

REDIS = redis.StrictRedis.from_url(
  os.environ.get("REDIS_URL", "redis://127.0.0.1:6379")
)


# @app.route("/", methods=["GET"])
# def hello():
#   return jsonify({"ok": True})

@app.route("/favicon.ico", methods=["GET"])
def favicon_ico():
  return Response(
    favicon,
    headers = {
      "Accept-Ranges": "bytes",
      "Content-Type": "image/vnd.microsoft.icon"
    }
  )

@app.route("/os/environ", methods=["GET"])
def environ():
  result = {}
  for key in os.environ.keys():
    result[key] = os.environ[key]
  return jsonify(result)

@app.route("/", methods=["GET"])
@app.route("/block/latest/height", methods=["GET"])
def block_latest_height():
  
  result = REDIS.get("height")
  
  if result:
    if len(result) > 0:
      return Response(
        result.decode(),
        headers = {
          "Content-Type": "application/json"
        }
      )

  r = requests.get("http://www.tokenview.com:8088/block/latest/height")
  if not r.ok:
    abort(500)
  
  REDIS.set("height", r.content)
  REDIS.expire("height", 60)
  
  return Response(
    r.content,
    headers = {
      "Content-Type": "application/json"
    }
  )


app.run(
  debug = False,
  host = "0.0.0.0",
  port = os.environ.get("PORT", 9999)
)

